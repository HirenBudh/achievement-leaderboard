import { Module } from '@nestjs/common';
import { UserAchievementService } from './user-achievement.service';
import { UserAchievementController } from './user-achievement.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserAchievement } from './entities/user-achievement.entity';
import { UsersModule } from 'src/users/users.module';
import { AchievementsModule } from 'src/achievements/achievements.module';

@Module({
  imports: [TypeOrmModule.forFeature([UserAchievement]), UsersModule, AchievementsModule],
  controllers: [UserAchievementController],
  providers: [UserAchievementService],
  exports: [UserAchievementService]
})
export class UserAchievementModule {}
