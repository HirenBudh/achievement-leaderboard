import { Controller, Get } from '@nestjs/common';
import { UserAchievementService } from './user-achievement.service';

@Controller('user-achievement')
export class UserAchievementController {
  constructor(private readonly userAchievementService: UserAchievementService) {}

  @Get()
  findAll() {
    return this.userAchievementService.findAll();
  }
}
