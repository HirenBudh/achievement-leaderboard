import { Achievement } from 'src/achievements/entities/achievement.entity';
import { User } from 'src/users/entities/user.entity';
import { Column, CreateDateColumn, Entity, JoinColumn, OneToOne, PrimaryColumn } from 'typeorm';

@Entity()
export class UserAchievement {
  @PrimaryColumn()
  id: number;

  @OneToOne(() => User, (user) => user.id)
  @JoinColumn()
  userId: number;

  @OneToOne(() => Achievement, (event) => event.id)
  @JoinColumn()
  achievementId: number;

  @Column()
  currentProgress: number;

  @CreateDateColumn()
  createdAt: string;
}
