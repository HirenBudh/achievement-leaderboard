import { Injectable, NotFoundException } from '@nestjs/common';
import { UserAchievement } from './entities/user-achievement.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UsersService } from 'src/users/users.service';
import { AchievementsService } from 'src/achievements/achievements.service';
import { CreateUserAchievementDto } from './dto/create-user-achievement.dto';

@Injectable()
export class UserAchievementService {
  constructor(
    @InjectRepository(UserAchievement)
    private userAchievementsRepository: Repository<UserAchievement>,
    private readonly usersService: UsersService,
    private readonly achievementService: AchievementsService,
  ) {}

  findAll() {
    return this.userAchievementsRepository.find();
  }

  findOne(userId: number, achievementId: number) {
    return this.userAchievementsRepository.findOne({
      where: { userId, achievementId },
    });
  }

  async update(id: number, value: number) {
    const userAchievement = await this.userAchievementsRepository.findOne({ where: { id } });
    userAchievement.currentProgress += value;
    return await this.userAchievementsRepository.save(userAchievement);
  }

  async create(createUserAchievemntDto: CreateUserAchievementDto) {
    const user = await this.usersService.findOne(
      createUserAchievemntDto.userId,
    );
    if (!user) {
      throw new NotFoundException('User not found');
    }

    const achievement = await this.achievementService.findOne(createUserAchievemntDto.achievementId);
    if (!achievement) {
      throw new NotFoundException('Achievement not found');
    }
    const userAchievement = this.userAchievementsRepository.create(createUserAchievemntDto);
    return this.userAchievementsRepository.save(userAchievement);
  }
}
