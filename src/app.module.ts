import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsersModule } from './users/users.module';
import { AchievementsModule } from './achievements/achievements.module';
import { TypeOrmModule } from '@nestjs/typeorm'
import { User } from './users/entities/user.entity';
import { Achievement } from './achievements/entities/achievement.entity';
import { EventsModule } from './events/events.module';
import { UserEventsModule } from './user-events/user-events.module';
import { UserAchievementModule } from './user-achievement/user-achievement.module';
import { UserEvent } from './user-events/entities/user-event.entity';
import { UserAchievement } from './user-achievement/entities/user-achievement.entity';
import { Event } from './events/entities/event.entity';

@Module({
  imports: [
    UsersModule,
    AchievementsModule,
    TypeOrmModule.forRoot({
      type: 'sqlite',
      database: './database.sqlite',
      entities: [Event, User, Achievement, UserEvent, UserAchievement],
      synchronize: true,
    }),
    EventsModule,
    UserEventsModule,
    UserAchievementModule,
],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
