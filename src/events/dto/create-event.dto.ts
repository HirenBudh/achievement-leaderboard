import { IsNotEmpty, IsNumber } from "class-validator";

export class CreateEventDto {
  @IsNotEmpty()
  description: string;

  @IsNotEmpty()
  @IsNumber()
  achievementId: number;

  @IsNotEmpty()
  @IsNumber()
  value: number
}
