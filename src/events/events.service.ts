import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { CreateEventDto } from './dto/create-event.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Event } from './entities/event.entity';
import { validate } from 'class-validator';
import { AchievementsService } from 'src/achievements/achievements.service';

@Injectable()
export class EventsService {
  constructor(
    @InjectRepository(Event)
    private eventsRepository: Repository<Event>,
    private readonly achievementsService: AchievementsService
  ){}
  async create(createEventDto: CreateEventDto) {
    const errors = await validate(createEventDto);
    if (errors.length > 0) {
      throw new BadRequestException(errors);
    }
    const achievement = await this.achievementsService.findOne(createEventDto.achievementId)
    if (!achievement) {
      throw new NotFoundException('Achivement not found');
    }
    const event = await this.eventsRepository.create(createEventDto);

    return this.eventsRepository.save(event);
  }

  findAll() {
    return this.eventsRepository.find();
  }

  findOne(id: number) {
    return this.eventsRepository.findOne({ where: { id }});
  }
}
