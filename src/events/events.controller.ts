import { Controller, Get, Post, Body, Patch, Param, Delete, Res } from '@nestjs/common';
import { EventsService } from './events.service';
import { CreateEventDto } from './dto/create-event.dto';
import { Response } from 'express';

@Controller('events')
export class EventsController {
  constructor(private readonly eventsService: EventsService) {}

  @Post()
  async create(@Body() createEventDto: CreateEventDto, @Res() res: Response) {
    try {
      const createdEvent = await this.eventsService.create(createEventDto);
      res.status(201).json(createdEvent);
    } catch (error) {
      res.status(500).json(error);
    }
  }

  @Get()
  async findAll(@Res() res: Response) {
    try {
      const events = await this.eventsService.findAll();
      res.status(200).json(events);
    } catch (error) {
      res.status(500).json(error)
    }
  }

  @Get(':id')
  async findOne(@Param('id') id: string, @Res() res: Response) {
    try {
      const events = await this.eventsService.findOne(+id);
      res.status(200).json(events);
    } catch (error) {
      res.status(500).json(error)
    }
  }
}
