import { Controller, Get, Post, Body, Patch, Param, Delete, Res } from '@nestjs/common';
import { AchievementsService } from './achievements.service';
import { CreateAchievementDto } from './dto/create-achievement.dto';
import { UpdateAchievementDto } from './dto/update-achievement.dto';
import { Response } from 'express';

@Controller('achievements')
export class AchievementsController {
  constructor(private readonly achievementsService: AchievementsService) {}

  @Post()
  async create(@Body() createAchievementDto: CreateAchievementDto, @Res() res: Response) {
    try {
      const createdAchievement = await this.achievementsService.create(createAchievementDto);
      res.status(201).json(createdAchievement);
    } catch (error) {
      res.status(500).json(error);
    }
  }

  @Get()
  findAll() {
    return this.achievementsService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.achievementsService.findOne(+id);
  }
}
