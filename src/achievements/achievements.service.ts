import { BadRequestException, Injectable } from '@nestjs/common';
import { CreateAchievementDto } from './dto/create-achievement.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Achievement } from './entities/achievement.entity';
import { validate } from 'class-validator';

@Injectable()
export class AchievementsService {
  constructor(
    @InjectRepository(Achievement)
    private achievementRepository: Repository<Achievement>
  ){}
  async create(createAchievementDto: CreateAchievementDto) {
    const errors = await validate(createAchievementDto);
    if (errors.length > 0) {
      throw new BadRequestException(errors);
    }
    const achievement = this.achievementRepository.create(createAchievementDto); 
    return this.achievementRepository.save(achievement);
  }

  findAll() {
    return this.achievementRepository.find();
  }

  async findOne(id: number) {
    return await this.achievementRepository.findOne({where: { id } });
  }
}
