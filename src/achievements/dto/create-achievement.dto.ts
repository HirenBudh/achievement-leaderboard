import { IsNotEmpty, IsNumber, IsPositive } from "class-validator";

export class CreateAchievementDto {
  @IsNotEmpty()
  name: string;
  @IsNotEmpty()
  description: string;

  @IsNotEmpty()
  @IsNumber()
  value: number;

  @IsNotEmpty()
  @IsNumber()
  goal: number;
}
