import { Column, CreateDateColumn, Entity, PrimaryColumn } from 'typeorm';

@Entity()
export class UserEvent {
  @PrimaryColumn()
  id: number;

  @Column()
  userId: number;

  @Column()
  eventId: number;

  @CreateDateColumn()
  createdAt: string;
}
