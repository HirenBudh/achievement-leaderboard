import { IsNotEmpty, IsNumber } from "class-validator";

export class CreateUserEventDto {
  @IsNotEmpty()
  @IsNumber()
  userId: number;

  @IsNotEmpty()
  @IsNumber()
  eventId: number;
}
