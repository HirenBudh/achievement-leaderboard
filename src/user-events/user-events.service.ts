import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateUserEventDto } from './dto/create-user-event.dto';
import { UserEvent } from './entities/user-event.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { EventsService } from 'src/events/events.service';
import { UsersService } from 'src/users/users.service';
import { AchievementsService } from 'src/achievements/achievements.service';
import { UserAchievementService } from 'src/user-achievement/user-achievement.service';

@Injectable()
export class UserEventsService {
  constructor(
    @InjectRepository(UserEvent)
    private userEventsRepository: Repository<UserEvent>,
    private readonly eventsService: EventsService,
    private readonly usersService: UsersService,
    private readonly achievementsService: AchievementsService,
    private readonly userAchievementsService: UserAchievementService,
  ) {}
  async create(createUserEventDto: CreateUserEventDto) {
    const user = await this.usersService.findOne(createUserEventDto.userId);
    if (!user) {
      throw new NotFoundException('User not found');
    }

    const event = await this.eventsService.findOne(createUserEventDto.eventId);
    if (!event) {
      throw new NotFoundException('Event not found');
    }

    const achievement = await this.achievementsService.findOne(
      event.achievementId,
    );

    const userAchievement = await this.userAchievementsService.findOne(
      user.id,
      achievement.id,
    );

    if (!userAchievement) {
      const newUserAchievement = await this.userAchievementsService.create({
        userId: user.id,
        achievementId: achievement.id,
        currentProgress: event.value,
      });
      console.log('did not', newUserAchievement);
    } else {
      console.log('existed', userAchievement);
      if (
        achievement.goal > userAchievement.currentProgress &&
        event.value >= achievement.goal - userAchievement.currentProgress
      ) {
        await this.usersService.update(user.id, achievement.value);
      }
    }

    return await this.userEventsRepository.create({
      ...createUserEventDto,
      createdAt: Date.now().toString(),
    });
  }
}
