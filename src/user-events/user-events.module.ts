import { Module } from '@nestjs/common';
import { UserEventsService } from './user-events.service';
import { UserEventsController } from './user-events.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserEvent } from './entities/user-event.entity';
import { EventsModule } from 'src/events/events.module';
import { UsersModule } from 'src/users/users.module';
import { AchievementsModule } from 'src/achievements/achievements.module';
import { UserAchievementModule } from 'src/user-achievement/user-achievement.module';

@Module({
  imports: [TypeOrmModule.forFeature([UserEvent]), EventsModule, UsersModule, AchievementsModule, UserAchievementModule],
  controllers: [UserEventsController],
  providers: [UserEventsService],
})
export class UserEventsModule {}
