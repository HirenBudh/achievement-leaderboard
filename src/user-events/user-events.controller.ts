import { Controller, Post, Body, Res } from '@nestjs/common';
import { UserEventsService } from './user-events.service';
import { CreateUserEventDto } from './dto/create-user-event.dto';
import { Response } from 'express';

@Controller('user-events')
export class UserEventsController {
  constructor(private readonly userEventsService: UserEventsService) {}

  @Post()
  async create(@Body() createUserEventDto: CreateUserEventDto, @Res() res: Response) {
    try {
      const userEvent = await this.userEventsService.create(createUserEventDto);
      res.status(201).json(userEvent);
    } catch (error) {
      res.status(500).json(error);
    }
  }
}
